extends KinematicBody2D

var MAX_SPEED = 200
var ACCELERATION = 1000
var DECCELERATION = 3000
var motion = Vector2()
var is_player = true

func _ready():
	close_labels()
	PlayerState.player_node = self
	$Camera2D/CanvasLayer/HUD.visible = true	
	
func _physics_process(delta):
	var axis = get_input_axis()
	if axis == Vector2.ZERO:
		apply_friction(DECCELERATION * delta)
	else:
		apply_movement(axis * ACCELERATION * delta)
	motion = move_and_slide(motion)
	
	if PlayerState.current_location != null && PlayerState.current_location == ProductivityController.productive_location:
		$NoiseLabels/Label1.text = ProductivityController.location_noises[ProductivityController.productive_location][0]
		$NoiseLabels/Label2.text = ProductivityController.location_noises[ProductivityController.productive_location][1]
		$NoiseLabels.show()
	elif !PlayerState.drinking_water && !PlayerState.eating:
		close_labels()

func drink_water():
	$NoiseLabels/Label1.text = "glug!"
	$NoiseLabels/Label2.text = "glug!"
	$NoiseLabels.show()
	
func eat_food():
	$NoiseLabels/Label1.text = "monch!"
	$NoiseLabels/Label2.text = "monch!"
	$NoiseLabels.show()
	
func close_labels():
	$NoiseLabels/Label1.set_text(" ")
	$NoiseLabels/Label2.set_text(" ")
	$NoiseLabels.hide()
	
func get_input_axis():
	var axis = Vector2.ZERO
	axis.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	axis.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	
	return axis.normalized()
	
func apply_friction(amount):
	if motion.length() > amount:
		motion -= motion.normalized() * amount
	else:
		motion = Vector2.ZERO
		$Anim.play("Idle")
		
func apply_movement(acceleration):
	motion += acceleration
	motion = motion.clamped(MAX_SPEED)
	if motion.x > 0:
		$Anim.play("Walk_Right")
	else:
		$Anim.play("Walk_Left")


func _on_Desk_body_entered(body):
	PlayerState.at_desk = true

func _on_Desk_body_exited(body):
	PlayerState.at_desk = false
	
