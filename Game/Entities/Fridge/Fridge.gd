extends Area2D

func _ready():
	self.connect("body_entered", self, "_on_body_entered")
	self.connect("body_exited", self, "_on_body_exited")

func _on_body_entered(_body):
	PlayerState.eating = true
	PlayerState.player_node.eat_food()	
	
func _on_body_exited(_body):
	PlayerState.eating = false
	PlayerState.player_node.close_labels()
