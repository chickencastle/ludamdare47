extends Sprite
var entities = {}


func _ready():
	set_process(false)

func _process(_delta):
	for entity in entities.values():
		if entity != null:
			if entity.global_position.y < self.global_position.y:
				entity.z_index = self.z_index - 1
			else:
				entity.z_index = self.z_index + 1
			
	if PlayerState.player_node.global_position.y < self.global_position.y:
		self.z_index = PlayerState.player_node.z_index + 1
	else:
		self.z_index = PlayerState.player_node.z_index - 1

func _on_Area2D_body_entered(body):
	if "is_npc" in body && body.is_npc:
		entities[body.name] = body
	set_process(true)


func _on_Area2D_body_exited(body):
	if "is_npc" in body && body.is_npc:
		entities[body.name] = null
	set_process(false)
