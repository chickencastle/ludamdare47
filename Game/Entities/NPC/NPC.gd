extends Node2D

export var speed = 45
export var can_move := true
export(Array) var _possible_destinations = ["Printer", "WaterCooler", "WaterCooler2", "WaterCooler3", "GreenSpace", "Meeting", "Fridge", "Netflix", "Desks"]
export(Color) var color = null
export(Texture) var texture = load("res://Assets/Textures/employee_onion.png")

var movement_enabled = false
var current_destination
var is_npc = true

var path := PoolVector2Array() setget set_path

func _ready():
	if can_move:
		randomize()
		$Timer.wait_time = randi()%15+5
		$Timer.start()
	$Sprite.texture = texture
	$Sprite.vframes = 2
	$Sprite.hframes = 8
	randomize()
	$Sprite.flip_h = randi()%1+0 == 1
	if color != null:
		$Sprite.modulate = color

func _process(delta):
	if PlayerState.player_node.global_position.y < self.global_position.y:
		self.z_index = PlayerState.player_node.z_index + 1
	else:
		self.z_index = PlayerState.player_node.z_index - 1
	if movement_enabled:
		var move_distance = speed * delta
		move_along_path(move_distance)
	
func move_along_path(distance):
	if path.size() == 0:
		movement_enabled = false
		$Timer.start()
		return
	if distance <= 0.0:
		return
	
	var distance_to_next_point : = position.distance_to(path[0])
	if distance_to_next_point > 0.0:
		position = position.linear_interpolate(path[0], min(distance, distance_to_next_point) / distance_to_next_point)
		set_animation(position.direction_to(path[0]))

	if distance >= distance_to_next_point:
		path.remove(0)
	
	move_along_path(distance - distance_to_next_point)
	
func set_animation(direction):
	if direction.x == 0 and direction.y == 0:
		$AnimationPlayer.play("Idle")
		return
		
	if direction.x > 0:
		$AnimationPlayer.play("Walk_Right")
	else:
		$AnimationPlayer.play("Walk_Left")
	
func set_path(value):
	path = value
	if value.size() == 0:
		return
	movement_enabled = true

func set_destination(destination_node_name):
	var path = NpcPathManager.get_npc_path(self, destination_node_name)
	current_destination = destination_node_name
	set_path(path)

func _on_Timer_timeout():
	var destinations_length = _possible_destinations.size()
	randomize()
	var rand = randi()%9+0
	set_destination(_possible_destinations[rand])
