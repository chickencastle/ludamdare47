extends Area2D

func _ready():
	self.connect("body_entered", self, "_on_body_entered")
	self.connect("body_exited", self, "_on_body_exited")
	
func _on_body_entered(_body):
	PlayerState.at_printer = true
	
func _on_body_exited(_body):
	PlayerState.at_printer = false
