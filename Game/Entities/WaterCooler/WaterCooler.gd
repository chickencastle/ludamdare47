extends Area2D

func _ready():
	self.connect("body_entered", self, "_on_body_entered")
	self.connect("body_exited", self, "_on_body_exited")
	
func _process(_delta):
	if PlayerState.player_node.global_position.y < self.global_position.y:
		self.z_index = PlayerState.player_node.z_index + 1
	else:
		self.z_index = PlayerState.player_node.z_index - 1

func _on_body_entered(_body):
	PlayerState.drinking_water = true
	PlayerState.player_node.drink_water()	
	
func _on_body_exited(_body):
	PlayerState.drinking_water = false
	PlayerState.player_node.close_labels()
