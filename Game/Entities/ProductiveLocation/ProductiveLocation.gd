extends Area2D

export(ProductivityController.Location) var location

func _ready():
	self.connect("body_entered", self, "_on_body_entered")
	self.connect("body_exited", self, "_on_body_exited")
	
func _on_body_entered(_body):
	PlayerState.current_location = location
	
func _on_body_exited(_body):
	PlayerState.current_location = null
