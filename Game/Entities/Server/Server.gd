extends Sprite

func _ready():
	ProductivityController.connect("productive_location_changed", self, "_on_ProductivityController_productive_location_changed")
	_on_ProductivityController_productive_location_changed()

func _on_ProductivityController_productive_location_changed():	
	if ProductivityController.productive_location == ProductivityController.Location.SERVER_ROOM:
		$AnimationPlayer.play("Broken")
	else:
		randomize()
		$AnimationPlayer.set_speed_scale(randf())
		$AnimationPlayer.play("Idle")

