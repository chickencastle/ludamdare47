extends Node

enum Location { DESK, PRINTER, SERVER_ROOM, MEETING, DAVE, PLANTS, CEO }
var location_dialogues = [
	"Get back to your desk!", 
	"The printer is busted!", 
	"Server room is overheating!", 
	"You're late for your meeting!",
	"Dave needs your help!",
	"The plants are dying!",
	"CEO wants a word!"
	]

var location_noises = {
	Location.DESK : ["click!", "clack!"],
	Location.PRINTER : ["bang!", "crack!"],
	Location.SERVER_ROOM : ["buzz!", "whirr!"],
	Location.MEETING : ["blah!", "blah!"],
	Location.DAVE : ["duh!", "ugh!"],
	Location.PLANTS : ["drip!", "drop!"],
	Location.CEO : ["yes!", "ok!"]
}

onready var productive_location = Location.DESK
var productive_destinations: Dictionary = {};
signal productive_location_changed

func initialize(destinations: Array):
	for destination in destinations:
		if "location" in destination:
			productive_destinations[destination.name] = destination
	emit_signal("productive_location_changed")
		
func _ready():
	TimeManager.connect("minute_passed", self, "_on_TimerManager_minute_passed")
	
func _on_TimerManager_minute_passed(minute):
	if minute == 0:
		var old_location = productive_location
		randomize()
		var rand = randi()%7+0
		productive_location = rand
		
		if old_location != productive_location:
			emit_signal("productive_location_changed")

