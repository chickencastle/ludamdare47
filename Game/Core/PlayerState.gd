extends Node

signal player_state_changed
signal player_productivity_changed
signal player_passed_out

onready var focus = 0
onready var hunger = 100
onready var thirst = 100
onready var energy = 100

onready var productivity = 0
var _base_productivity_rate = 100
var total_score = 0
var daily_scores = {}

var _focus_depreciation = 20 # loose focus fast
var _focus_appreciation = 10
var focus_multiplier = 1

var _thirst_depreciation = 1.75 
var _thirst_appreciation = 30

var _hunger_depreciation = 1 
var _hunger_appreciation = 15

var _energy_depreciation = 1 
var _energy_appreciation = 1.25

var current_location
var at_desk = false
var at_printer = false
var drinking_water = false
var eating = false

var player_node
var player_name
var passed_out = false

func _ready():
	SilentWolf.configure({
				"api_key": "lgzNnPmbO85KeyrUM1LPqa4VmKfHspnh5sB190Xk",
				"game_id": "LD47",
				"game_version": "1.0.0",
				"log_level": 1
			  })
	SilentWolf.configure_scores({
		"open_scene_on_close": "res://Scenes/TitleMenu/TitleMenu.tscn"
	})
	TimeManager.connect("minute_passed", self, "_on_TimerManager_minute_passed")
	
func _on_TimerManager_minute_passed(minute):
	if not passed_out:	
		check_hunger()
		check_thirst()
		check_energy()
		check_focus()
		if int(minute) % 10 == 0:
			check_productivity()
	
	emit_signal("player_state_changed")

func check_productivity():
	if current_location != ProductivityController.productive_location:
		return
		
	if focus == 100:
		focus_multiplier = 10
	elif focus >= 90:
		focus_multiplier = 5
	elif focus >= 70:
		focus_multiplier = 4
	elif focus >= 60:
		focus_multiplier = 3
	elif focus >= 50:
		focus_multiplier = 2
	else:
		focus_multiplier = 1
	
	if not passed_out:
		productivity += _base_productivity_rate * focus_multiplier
		emit_signal("player_productivity_changed")

func check_focus():
	var hunger_multiplier = _focus_depreciation * ((100 - hunger) / 100) * 1 # hunger not as distracting until lower levels
	var thirst_multiplier = _focus_depreciation * ((100 - thirst) / 100) * .8
	var fVal = hunger_multiplier + thirst_multiplier
	
	if current_location == ProductivityController.productive_location:
		focus = clamp((focus + _focus_appreciation - fVal), 0, 100)
	else: 
		focus = clamp((focus - fVal), 0, 100)

func check_hunger():
	if eating:
		hunger = clamp((hunger + _hunger_appreciation), 0, 100)
		energy = clamp((energy + _energy_appreciation), 0, 100)
	else:
		hunger = clamp(hunger - _hunger_depreciation, 0, 100)
		
func check_thirst():
	if drinking_water:
		thirst = clamp((thirst + _thirst_appreciation), 0, 100)
	else:
		thirst = clamp(thirst - _thirst_depreciation, 0, 100)
		
func check_energy():
	if energy > 0:
		energy -= _energy_depreciation
	else:
		passed_out = true
		total_score += productivity
		daily_scores[TimeManager.current_time["day"]] = productivity
		emit_signal("player_passed_out")
		
func reset_state():
	current_location = null
	focus_multiplier = 1
	drinking_water = false
	eating = false
	productivity = 0
	focus = 0
	hunger = 100
	thirst = 100
	energy = 100
