extends Node

var _timer = null
onready var current_time = { "day": 1, "hour": 8, "minute": 00, "meridiem": "am" }
signal minute_passed

func try_initialize():
	if _timer == null:
		_timer = Timer.new()
		add_child(_timer)
		_timer.connect("timeout", self, "_on_Timer_timeout")
		_timer.set_wait_time(1.0)
		_timer.set_one_shot(false) # Make sure it loops
		_timer.start()
	
func _on_Timer_timeout():
	if current_time["minute"] == 55:
		if current_time["hour"] == 12:
			current_time["hour"] = 1
		else:
			current_time["hour"] = current_time["hour"] + 1
			if current_time["hour"] == 12:
				if current_time["meridiem"] == "pm":
					current_time["meridiem"] = "am"
				else:
					current_time["meridiem"] = "pm"
		current_time["minute"] = 0
	else:
		current_time["minute"] = current_time["minute"] + 5
	emit_signal("minute_passed", current_time["minute"])

func next_day():
	current_time["day"] += 1
	current_time["minute"] = 00
	current_time["hour"] = 8
	current_time["meridiem"] = "am"
