extends Node2D

var nav_2d: Navigation2D;
var destinations: Dictionary = {};

func initialize(navigation2d: Navigation2D, destinations: Array):
	nav_2d = navigation2d
	for destination in destinations:
		self.destinations[destination.name] = destination

func get_npc_path(from_node: Node, to_node_name: String):
	if not destinations.has(to_node_name):
		return
	var position_to_go_to
	var children = destinations[to_node_name].get_children()
	if children == null || children.size() == 0:
		position_to_go_to = destinations[to_node_name].global_position
	else:
		randomize()
		var point_children = []
		for item in children:
			if("Point" in item.name):
				point_children.append(item)
		if point_children.size() == 0:
			position_to_go_to = destinations[to_node_name].global_position
		else:
			var rand = randi()%(point_children.size() - 1)+0
			position_to_go_to = point_children[rand].global_position
	return nav_2d.get_simple_path(from_node.global_position, position_to_go_to)
