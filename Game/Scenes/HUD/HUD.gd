extends Control

onready var focus_bar =  $Margin/VBox/TopBar/Stats/FocusBar
onready var hunger_bar =  $Margin/VBox/TopBar/Stats/HBoxContainer/HungerBar
onready var thirst_bar =  $Margin/VBox/TopBar/Stats/HBoxContainer/ThirstBar
onready var energy_bar =  $Margin/VBox/TopBar/Time/EnergyBar
onready var productivity =  $Margin/VBox/TopBar/Productivity/ProdScoreValue
onready var productivity_increase =  $Margin/VBox/TopBar/Productivity/Score/ScoreIncreaseLabel
onready var multiplier =  $Margin/VBox/TopBar/Productivity/Score/MultiplierLabel

func _ready():
	$Margin/VBox/TopBar/Time/LocationHelper.set_text(" ")
	productivity_increase.set_text(" ")
	multiplier.set_text(" ")
	$Margin/VBox/TopBar/Time/HBoxContainer/DayValue.text = "Day " + str(TimeManager.current_time["day"])
	productivity.text = str(int(PlayerState.productivity))
	PlayerState.connect("player_state_changed", self, "_on_PlayerState_player_state_changed")
	PlayerState.connect("player_productivity_changed", self, "_on_PlayerState_player_productivity_changed")
	_on_PlayerState_player_state_changed()
	
func _process(_delta):
	var time_label = $Margin/VBox/TopBar/Time/HBoxContainer/TimeValue
	var hour = str(TimeManager.current_time["hour"])
	var minute = str(TimeManager.current_time["minute"])
	if minute.length() < 2:
		minute = "0" + minute
	time_label.text = hour + ":" + minute + TimeManager.current_time["meridiem"]
	set_location_helper()
	

func _on_PlayerState_player_state_changed():
	focus_bar.set_value(PlayerState.focus)
	hunger_bar.set_value(PlayerState.hunger)
	thirst_bar.set_value(PlayerState.thirst)
	energy_bar.set_value(PlayerState.energy)
	
func _on_PlayerState_player_productivity_changed():
	productivity_increase.text = "+" + str(int(PlayerState.productivity) - int(productivity.text))
	if PlayerState.focus_multiplier == 10:
		multiplier.text = "x" + str(int(PlayerState.focus_multiplier)) + " FLOW"
	elif PlayerState.focus_multiplier >= 2:
		multiplier.text = "x" + str(int(PlayerState.focus_multiplier)) + " FOCUS BONUS"
	else:
		multiplier.text = ""
	productivity.text = str(int(PlayerState.productivity))
	$Margin/VBox/TopBar/Productivity/ProductivityAnimator.play("Pulse")

func set_location_helper():
	var needs_label = $Margin/VBox/TopBar/Stats/NeedsHelper;
	if PlayerState.current_location == null || int(PlayerState.current_location) != int(ProductivityController.productive_location):
		$Margin/VBox/TopBar/Time/LocationHelper.text = ProductivityController.location_dialogues[ProductivityController.productive_location]
		needs_label.set_text(" ")
	else:
		$Margin/VBox/TopBar/Time/LocationHelper.set_text(" ")
		needs_label.text = "You're being productive!"
	if PlayerState.thirst < 40 || PlayerState.hunger < 30:
		needs_label.text = "You're losing focus!"
