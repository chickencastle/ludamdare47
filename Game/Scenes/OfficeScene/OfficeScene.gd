extends Node2D

func _ready():	
	NpcPathManager.initialize($Navigation2D, $Destinations.get_children())
	ProductivityController.initialize($Destinations.get_children())
	$CanvasLayer/FadeIn.fade_in()
	$CanvasLayer/FadeIn.show()
	PlayerState.connect("player_passed_out", self, "_on_PlayerState_player_passed_out")

func _on_PlayerState_player_passed_out():
	$CanvasLayer/FadeIn.show()
	$CanvasLayer/FadeIn.fade_out()

func _on_FadeIn_fade_in_finished():
	TimeManager.try_initialize()
	$CanvasLayer/FadeIn.hide()
	PlayerState.passed_out = false

func _on_FadeIn_fade_out_finished():
	get_tree().change_scene("res://Scenes/OvernightMenu/OvernightMenu.tscn")
