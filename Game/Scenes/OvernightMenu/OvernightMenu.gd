extends Control

var scene_path_to_load

func _ready():
	$Music.play()
	$Menu/Scores/Today/TodaysScoreValue.text = str(int(PlayerState.productivity)) + " KPI's"
	$Menu/Scores/Total/TotalScoreValue.text = str(int(PlayerState.total_score)) + " KPI's"
	var highest_score = 0
	var highest_score_day = 1
	for day in PlayerState.daily_scores.keys():
		var score = PlayerState.daily_scores[day]
		if score > highest_score:
			highest_score = score
			highest_score_day = day
	$Menu/Scores/BestDay/BesyDayValue.text = str(int(highest_score)) + " KPI's"


func _on_FadeIn_fade_finished():
	get_tree().change_scene(scene_path_to_load)


func _on_NewGameButton_pressed():
	$Music.stop()
	PlayerState.reset_state()
	TimeManager.next_day()
	load_scene("res://Scenes/OfficeScene/OfficeScene.tscn")


func load_scene(sceneToLoad):
	scene_path_to_load = sceneToLoad
	$FadeIn.show()
	$FadeIn.fade_out()

func _on_GiveUpButton_pressed():
	SilentWolf.Scores.persist_score(PlayerState.player_name, str(int(PlayerState.total_score)))
	load_scene("res://Scenes/TitleMenu/TitleMenu.tscn")
