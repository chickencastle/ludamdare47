extends Control

var scene_path_to_load
var player_name

func _ready():
	Music.get_node("DrumLine").play()
	yield(get_tree().create_timer(0.1), "timeout")
	yield(SilentWolf.Scores.get_high_scores(), "sw_scores_received")
	$Menu/HighScores/HighScoreAnimator.stop()
	$Menu/HighScores/Label.set("modulate", Color(1, 1, 1, 1))
	$Menu/HighScores/Label.text = "Employees of the month!"
	print("Scores: " + str(SilentWolf.Scores.scores))
	var num_scores = 0
	for score_data in SilentWolf.Scores.scores:
		if num_scores < 5:
			var hBox = preload("res://Resources/UI/PlayerScoreBox/PlayerScoreBox.tscn").instance()
			hBox.get_node("PlayerName").text = str(score_data.player_name)
			hBox.get_node("PlayerScore").text = str(score_data.score) + " KPI's"
			$Menu/HighScores.add_child(hBox)
			num_scores += 1

func load_leaderboard_screen():
	get_tree().change_scene("res://addons/silent_wolf/Scores/Leaderboard.tscn")
	
func _on_FadeIn_fade_finished():
	get_tree().change_scene(scene_path_to_load)

func _on_NewGameButton_pressed():
	if player_name != null:
		load_scene("res://Scenes/OfficeScene/OfficeScene.tscn")
		PlayerState.player_name = player_name

func load_scene(sceneToLoad):
	scene_path_to_load = sceneToLoad
	$FadeIn.show()
	$FadeIn.fade_out()

func _on_LineEdit_text_changed(new_text):
	player_name = new_text

func _on_LeaderboardButton_pressed():
	yield(SilentWolf.Scores.get_high_scores(), "sw_scores_received")
	print("Scores: " + str(SilentWolf.Scores.scores))
