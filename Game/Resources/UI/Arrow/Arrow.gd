extends Node2D

var _target

const ROTATION_SPEED = 100; 

func _process(delta): 
	if ProductivityController.productive_location != null:
		for dest in ProductivityController.productive_destinations.values():
			if int(dest.location) == int(ProductivityController.productive_location):
				_target = dest
	if _target != null:
		var vector_towards_target = _target.global_position - self.global_position; 
		var angle = (atan2(vector_towards_target.y, vector_towards_target.x)); 
		rotation = lerp(rotation, angle, delta * ROTATION_SPEED);
	if PlayerState.current_location == ProductivityController.productive_location:
		$AnimationPlayer.stop()		
		$Sprite.hide()
	else:
		$AnimationPlayer.play("Flash")
		$Sprite.show()
		
func _ready():
	ProductivityController.connect("productive_location_changed", self, "_on_ProductivityController_productive_location_changed")

func _on_ProductivityController_productive_location_changed():	
	_target = null
	for dest in ProductivityController.productive_destinations.values():
		if int(dest.location) == int(ProductivityController.productive_location):
			_target = dest
