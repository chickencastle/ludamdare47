extends ProgressBar

export(Color) var bar_color;
export(String) var text;
export(Dictionary) var tiers;
export(int) var value_tolerance = 25

func _ready():
	$Anim.stop(true)
	var style_box = StyleBoxFlat.new()
	style_box.set_bg_color(bar_color)
	style_box.set_border_width_all(2)
	style_box.set_corner_radius_all(8)
	style_box.set_border_color(Color(1, 1, 1))
	set("custom_styles/fg", style_box)
	$Label.text = text

func set_value(new_value):
	value = new_value

func _process(_delta):
	if tiers != null:
		var tranches = tiers.keys()
		tranches.sort()
		var matched = false
		for tranch in tranches:
			if value >= tranch:
				$Label.text = tiers[tranch]
				matched = true
		if not matched:
			$Label.text = text
	if value <= value_tolerance:
		$Anim.play("DangerFlash")
	elif value > value_tolerance:
		$Label.set("custom_colors/font_color", Color(1,1,1,1))
		$Anim.stop(true)
